package com.fafica.gustavo.atividade;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.fafica.gustavo.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirm(v);
            }
        });
    }

    private void confirm(View view) {
        Intent intent = new Intent(this, ConfirmActivity.class);

        EditText editTextNome = (EditText) findViewById(R.id.editText7);
        EditText editTextSobrenome = (EditText) findViewById(R.id.editText8);
        EditText editTextTelefone = (EditText) findViewById(R.id.editText9);

        RadioGroup rg = (RadioGroup) findViewById(R.id.rg);
        int radioButtonID = rg.getCheckedRadioButtonId();
        View radioButton = rg.findViewById(radioButtonID);
        int idx = rg.indexOfChild(radioButton);
        RadioButton r = (RadioButton)  rg.getChildAt(idx);
        String selectedtext = r.getText().toString();

        String message = "Nome: " + editTextNome.getText().toString() + " Sobrenome: " + editTextSobrenome.getText().toString() + "\n" + "Sexo: " + selectedtext + "\n" + "Telefone: " + editTextTelefone.getText().toString();

        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
}
